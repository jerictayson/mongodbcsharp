using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace MongoDBCSharp.Models;

public class Employee
{
    
    public ObjectId Id { get; set; }
    [Required]
    public string? firstName { get; set; }
    [Required]
    public string? lastName { get; set; }
    [Required]
    public string? address { get; set; }
    public string? birthDate { get; set; }
    public Credential credential { get; set; }
}