using System.ComponentModel.DataAnnotations;

namespace MongoDBCSharp.Models;

public class Credential
{
    [Required]
    [DataType(DataType.EmailAddress)]
    public string email { get; set; }
    [Required]
    public string username { get; set; }
    [Required]
    public string password { get; set; }
}