﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using System.Text.RegularExpressions;
using MongoDB.Driver;
using MongoDBCSharp.Models;

namespace MongoDBCSharp;

internal abstract class Program
{
    private static bool _isLoggedIn;
    private static Database? _dbConn;

    private static void Main()
    {
        _dbConn = new Database();

        while (true)
            if (!_isLoggedIn)
                Login();
            else
                try
                {
                    Console.WriteLine("Welcome to Employee Management System");
                    Console.WriteLine("Please enter your choices");
                    Console.WriteLine("1. Insert Employee");
                    Console.WriteLine("2. Delete Employee");
                    Console.WriteLine("3. Update Employee");
                    Console.WriteLine("4. View Employees");
                    Console.WriteLine("5. Logout");
                    Console.Write("Enter your choice: ");
                    var choice = Convert.ToInt32(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            InsertData();
                            break;
                        case 2:
                            DeleteEmployee();
                            break;
                        case 3:
                            UpdateEmployee();
                            break;
                        case 4:
                            ViewEmployees();
                            break;
                        case 5:
                            _isLoggedIn = false;
                            break;
                        default:
                            Console.WriteLine("Invalid choice");
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid choice");
                }
    }

    private static void Login()
    {
        Console.Write("Enter username: ");
        var username = Console.ReadLine()?.Trim();
        Console.Write("Enter password: ");
        var password = Console.ReadLine()?.Trim();
        if (_dbConn != null) _isLoggedIn = _dbConn.CheckEmployee(username, password);
        if (!_isLoggedIn) Console.WriteLine("Invalid username or password");
    }

    private static void UpdateEmployee()
    {
        ViewEmployees();
        Console.Write("Enter employee username: ");
        var username = Console.ReadLine()?.Trim();
        var filter = Builders<Employee>.Filter.Eq("credential.username", username);
        var employee = _dbConn?.Employees.Find(filter).FirstOrDefault();
        if (employee == null)
        {
            Console.WriteLine("Employee not found");
            return;
        }
        
        Console.Write("Enter new first name: ");
        var firstName = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(firstName)) employee.firstName = firstName;
        Console.Write("Enter new last name: ");
        var lastName = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(lastName)) employee.lastName = lastName;
        Console.Write("Enter new address: ");
        var address = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(address)) employee.address = address;
        Console.Write("Enter new birth date: ");
        var birthDate = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(birthDate)) employee.birthDate = birthDate;
        Console.Write("Enter new email: ");
        var email = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(email)) employee.credential.email = email;
        Console.Write("Enter new username: ");
        var newUsername = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(newUsername)) employee.credential.username = newUsername;
        Console.Write("Enter new password: ");
        var newPassword = Console.ReadLine()?.Trim();
        if (!string.IsNullOrEmpty(newPassword)) employee.credential.password = newPassword;

        Console.WriteLine(_dbConn?.UpdateEmployee(employee.credential.username, employee));

    }

    private static void DeleteEmployee()
    {
        ViewEmployees();
        Console.Write("Enter employee username: ");
        var username = Console.ReadLine()?.Trim();
        var filter = Builders<Employee>.Filter.Eq("credential.username", username);
        Console.WriteLine(_dbConn?.DeleteEmployee(username));
    }

    private static void ViewEmployees()
    {
        var res = _dbConn?.Employees.Find(Builders<Employee>.Filter.Empty).ToList();
        Debug.Assert(res != null, nameof(res) + " != null");
        foreach (var employee in res)
        {
            Console.WriteLine("Employee Id: " + employee.Id);
            Console.WriteLine("First Name: " + employee.firstName);
            Console.WriteLine("Last Name: " + employee.lastName);
            Console.WriteLine("Address: " + employee.address);
            Console.WriteLine("Birth Date: " + employee.birthDate);
            Console.WriteLine("Email: " + employee.credential.email);
            Console.WriteLine("Username: " + employee.credential.username);
            Console.WriteLine("Password: " + employee.credential.password);
            Console.WriteLine();
        }
    }

    private static void InsertData()
    {
        while (true)
        {
            Console.WriteLine("Please enter the employee details");
            Console.Write("Enter first name: ");
            var firstName = Console.ReadLine()?.Trim();
            Console.Write("Enter last name: ");
            var lastName = Console.ReadLine()?.Trim();
            Console.Write("Enter address: ");
            var address = Console.ReadLine()?.Trim();
            Console.Write("Enter birth date: ");
            var birthDate = Console.ReadLine()?.Trim();
            Console.Write("Enter email: ");
            var email = Console.ReadLine()?.Trim();
            Console.Write("Enter username: ");
            var username = Console.ReadLine()?.Trim();
            Console.Write("Enter password: ");
            var password = Console.ReadLine()?.Trim();

            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) ||
                string.IsNullOrEmpty(address) || string.IsNullOrEmpty(birthDate) ||
                string.IsNullOrEmpty(email) || string.IsNullOrEmpty(username)
                || string.IsNullOrEmpty(password))
            {
                Console.WriteLine("Please enter all the fields");
                return;
            }

            //check if the email is valid
            var emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (!emailRegex.IsMatch(email))
            {
                Console.WriteLine("Please enter a valid email");
                return;
            }

            //check if the birth date is valid with regex (mm/dd/yyyy)
            var birthDateRegex = new Regex(@"^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]\d{4}$");
            if (!birthDateRegex.IsMatch(birthDate))
            {
                Console.WriteLine("Please enter a valid birth date");
                return;
            }

            var employee = new Employee
            {
                firstName = firstName,
                lastName = lastName,
                address = address,
                birthDate = birthDate,
                credential = new Credential
                {
                    email = email,
                    username = username,
                    password = password
                }
            };
            if (_dbConn != null) Console.WriteLine(_dbConn.InsertEmployee(employee));
            break;
        }
    }
}