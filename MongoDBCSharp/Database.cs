using MongoDB.Driver;
using MongoDBCSharp.Models;

namespace MongoDBCSharp;

public class Database
{
    private readonly IMongoDatabase _database;

    public Database()
    {
        var client = new MongoClient("mongodb://localhost:27017");
        _database = client.GetDatabase("EmployeeManagementSystem");
    }
    
    public IMongoCollection<Employee> Employees =>
        //return a list of employees
        _database.GetCollection<Employee>("employee");

    public string InsertEmployee(Employee employee)
    {
        Employees.InsertOne(employee);
        return "Employee inserted successfully";
    }
    
    public string DeleteEmployee(string username)
    {
        var filter = Builders<Employee>.Filter.Eq("credential.username", username);
        if(filter == null) return "Employee not found";
        Employees.DeleteOne(filter);
        return "Employee deleted successfully";
    }
    
    public string UpdateEmployee(string username, Employee employee)
    {
        var filter = Builders<Employee>.Filter.Eq("credential.username", username);
        var update = Builders<Employee>.Update
            .Set("firstName", employee.firstName)
            .Set("lastName", employee.lastName)
            .Set("address", employee.address)
            .Set("birthDate", employee.birthDate)
            .Set("credential", employee.credential);
        Employees.UpdateOne(filter, update);
        return "Employee updated successfully";
    }
    
    public bool CheckEmployee(string? username, string? password)
    {
        var filter = Builders<Employee>.Filter.Eq("credential.username", username);
        var result = Employees.Find(filter).FirstOrDefault();
        if (result == null)
        {
            return false;
        }

        if (result.credential.password == password)
        {
            return true;
        }

        return false;
    }
}